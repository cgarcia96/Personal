var elixir = require('laravel-elixir');

//Este contiene urlAdjutes, rename, prependRelative
require('laravel-elixir-helpers');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'resources/assets/backend/fonts');
    mix.copy('node_modules/jquery/dist', 'resources/assets/backend/external/jquery');
});


elixir(function(mix) {
    mix.sass([
        'app.scss'
    ], 'resources/assets/backend/css');
});
/*
elixir(function(mix){
    mix.copy('resources/assets/backend/bootstrap/css/bootstrap.css', 'public/build/backend/css');
    mix.copy('resources/assets/backend/bootstrap/js/bootstrap.js', 'public/build/backend/js');
    mix.copy('resources/assets/backend/jquery/jquery.js', 'public/build/backend/js');
});
*/


elixir(function(mix){
    mix.styles([
        '../backend/css/app.css',
        '../backend/external/bootstrap/css/bootstrap.css'
    ], 'public/backend/css');
});

elixir(function(mix){
    mix.scripts([
        '../backend/external/jquery/jquery.js'
    ], 'public/backend/js' );
});
elixir(function(mix) {
    mix.version([
        "public/backend/css/all.css",
        "public/backend/js/all.js"
    ]);
});