<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        @include('backend.layouts.head_content')
        @include('backend/layouts/include_js')
    </head>
    <body>
        <!--
        <header>

        </header>
        -->

        <section class="container">
            @yield('content')
        </section>

        <!--
        <footer>

        </footer>
        -->
    </body>
</html>