@extends('backend.layouts.master')
@section('title', 'Bienvenido')

@section('content')
    <section class="container">
        <div style="margin: 20px 0px; text-align: center;" >
            <form id="form-login" class="form-inline">
                <fieldset>
                    <div class="form-group">
                        <label class="label-control col-sm-6" name="username" for="username">Username</label>
                        <div class="col-sm-6">
                            <input type="text" name="username" class="form-control">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="label-control col-sm-6" name="password" for="password">Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="password" class="form-control">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>

                    <button class=" btn btn-primary" >Ingresar</button>
                </fieldset>
            </form >
        </div>


    </section>
    <script type="text/javascript">

        $(document).ready(function(){
            loadEvent();
        })


        function loadEvent(){
            $('#form-login').on('submit', function(e){
                e.preventDefault();

                var inputs = $(this).find('input');
                var data = {};

                $.each(inputs, function(key,input){
                    data[input.name] = input.value;
                });


                $.ajax({
                    url: "https://quadminds-test-login.getsandbox.com/login",
                    type: "POST",
                    contentType : 'application/json',
                    data: JSON.stringify(data),
                    success: function(response){
                        alert(response);

                    }

                })
            });
        }

    </script>
@endsection